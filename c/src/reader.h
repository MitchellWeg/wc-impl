#ifndef READER_H
#define READER_H

typedef struct FileStats {
    int word_count;
    size_t total_bytes_read;
} FileStats;

int reader_line_by_line(FILE *file, FileStats* fs);
int reader_read_file(char* filename);

#endif