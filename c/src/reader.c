#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "reader.h"

#define WHITESPACE_CODE 32

int reader_read_file(char* filename) {
    int return_code = 0;

    FILE *file;

    file = fopen(filename, "r");

    // Could not get the file handle
    if(!file) {
        return -1;
    }

    FileStats *fs = malloc(sizeof(FileStats));

    return_code = reader_line_by_line(file, fs);

    printf("Bytes read: %zu, Word count: %d\n", fs->total_bytes_read, fs->word_count);

    fclose(file);

    free(fs);

    return return_code;
}

/*
 * Read the file line by line.
 *
 * A word starts with a letter. 
 * Read until the next whitespace is found.
 * If the last char read was an actual character, we've found a word.
 * Otherwise, we were just reading a bunch of whitespaces.  
 */
int reader_line_by_line(FILE *file, FileStats *fs) {
    fs->total_bytes_read = 0;
    fs->word_count = 0;

    bool char_read = false;
    int c;

    while ((c = getc(file)) != EOF) {
        switch(c) {
            case WHITESPACE_CODE: {
                if(char_read) {
                    fs->word_count++;
                    char_read = false;
                }

                break;
            }
            default: {
                char_read = true;
                break;
            }
        }

        fs->total_bytes_read++;
    }

    // EDGE CASE:
    // We were done reading the file, 
    // but the last word was not terminated by a whitespace.
    if(char_read) {
        fs->word_count++;
    }

    return 0;
}