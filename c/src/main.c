#include <stdio.h>

#include "reader.h"

int main(int argc, char** argv) {
    if(argc <= 1) {
        printf("not enough arguments supplied...\n");
        return -1;
    }

    char* filename = argv[1];

    int code = reader_read_file(filename);

    switch(code) {
        case -1: {
            printf("Could not obtain file handle, does the file '%s' exist?", filename);
            break;
        }
        default: break; // no-op
    }

    return code;
}