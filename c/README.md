# Word count C implementation

## How to run
Create a build dir:

```
mkdir build && cd build 
```

Then use CMake to build the project:

```
cmake .. && make
```

Then you can run the project:

```
./wc-impl <path to file>
```